# <center> AR Remake of Ball Breaker </center>

![](./backend/assets/brick-breaker.png)

Ball Breaker was an isometric breakout style of game that was released back in 1987 by CRL for the Amstrad and ZX Spectrum platforms.This is a remake of that game but with AR capabilities

## Gameplay

![](./backend/assets/gameplay.gif.gif)

## Dependencies

- `openCV C++ libraries` : For fist detection
- `make` : Compilation automation
- `premake` : Build Environment
- `SFML`: GUI interfac

> Included Fist detection classifier

## How to Build

Clone the repo

```
git clone https://gitlab.com/HolyChicken99/ballbreaker
```

Change the paths of the libraries (SFML and OpenCV) in Premake5.lua

```
cd ballbreaker/frontend
emacs premake5.lua
```

Build the makefile for our project using Premake5

```
premake5 gmake2
```

Run the makefile to build project

```
make
```

Execute the built binary

```
cd /bin
./ballbreaker
```

- Make a fist in front of the webcam
- Move it Left / Right to move the paddle accordingly

### LINUX USERS

[OpenCV installation](https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html)
:warning:
Building OpenCV is really hard on linux especially for Arch based

### Links

> The best Build tool

[premake5](https://premake.github.io/)

## License

The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.
