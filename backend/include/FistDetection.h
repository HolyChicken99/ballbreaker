//
// Created by alpha on 2021-11-06.
//
#ifndef BALLBREAKER_FISTDETECTION_H
#define BALLBREAKER_FISTDETECTION_H

#include <opencv2/opencv.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <stdexcept>
#include <vector>
#include <iostream>
#include<utility>



///\Path path to xml file
#define XML_PATH "/opt/opencv/opencv.xml"
///\Path path to test image
#define TEST_IMAGE_PATH "/home/alpha/Documents/projects/ballbreaker/backend/assets/fist_image.jpg"

using namespace cv;

typedef struct {
    int x;
    int y;
} point;


class FistDetection {
    CascadeClassifier fistCascade;
    Mat img;
    VideoCapture cap;
    std::vector<cv::Rect> fists;
public :

    // initialize camera mode
    void init();
    // test to see if all functions work (uses static image)
    void test();
    // test to if the camera functionality is working
    void cameraTest();
    // update loop to print out the x position of the fist
    void update();
    // returns the position fist 
    int capture();
};


#endif //BALLBREAKER_FISTDETECTION_H
