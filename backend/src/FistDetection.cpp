//
// Created by alpha on 2021-11-06.
//
#include "../include/FistDetection.h"

void FistDetection::init() {
    fistCascade.load(XML_PATH);
    if (fistCascade.empty()) {
        std::cerr << "wrong XML FILE fed " << '\n';
        return;
    }
    cap.open(0);
}

int FistDetection::capture() {
    // return the x co-ordinate of the hand
    cap.read(img);
    fistCascade.detectMultiScale(img, fists);
//    rectangle(img, fists[0].tl(), fists[0].br(), Scalar(120, 255, 167), 1);
//    imshow("image", img);
    waitKey(1);
    if (!fists.empty()) {
        return fists[0].tl().x;
    }
    return 0;
}

void FistDetection::test() {
    fistCascade.load(XML_PATH);
    std::string path{TEST_IMAGE_PATH};
    img = imread(path);

    if (fistCascade.empty()) {
        std::cerr << "invalid XML file read" << '\n';
    }

    fistCascade.detectMultiScale(img, fists);

    for (auto &fist: fists) {
        rectangle(img, fist.tl(), fist.br(), Scalar(120, 255, 167), 1);
    }
    imshow("image", img);
    waitKey(0);
}

void FistDetection::update() {
    while (true) {
        std::cout << " x : " << this->capture() << "\n";
    }
}

void FistDetection::cameraTest() {
    cap.open(0);
    cap.read(img);
    imshow("camera", img);
    waitKey(0);


}
