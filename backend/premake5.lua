workspace "ballBreaker"
configurations { "Debug", "Release" }
project "ballBreaker"

kind "ConsoleApp"
language "C++"
includedirs {"/usr/include/opencv4/","./include/ "}

links {"opencv_gapi", "opencv_stitching"," opencv_aruco"," opencv_barcode"," opencv_bgsegm"," opencv_bioinspired"," opencv_ccalib",  "opencv_dnn_objdetect"," opencv_dnn_superres", "opencv_dpm", "opencv_face", "opencv_freetype", "opencv_fuzzy", "opencv_hfs" }

links {"opencv_img_hash", "opencv_intensity_transform", "opencv_line_descriptor", "opencv_mcc", "opencv_quality", "opencv_rapid", "opencv_reg", "opencv_rgbd", "opencv_saliency", "opencv_stereo", "opencv_structured_light", "opencv_phase_unwrapping", "opencv_superres", "opencv_optflow", "opencv_surface_matching", "opencv_tracking", "opencv_highgui", "opencv_datasets", "opencv_text", "opencv_plot", "opencv_videostab", "opencv_videoio", "opencv_wechat_qrcode", "opencv_xfeatures2d", "opencv_shape", "opencv_ml", "opencv_ximgproc", "opencv_video", "opencv_xobjdetect", "opencv_objdetect", "opencv_calib3d", "opencv_imgcodecs", "opencv_features2d", "opencv_dnn", "opencv_flann", "opencv_xphoto", "opencv_photo", "opencv_imgproc", "opencv_core" }


targetdir "bin/%{cfg.buildcfg}"
libdirs { "~/Documents/Headers/build/lib/ "}

warnings "Extra"

files { "./src/**.cpp" }
   filter "configurations:Debug"
       defines { "DEBUG" }
       symbols "On"
   filter "configurations:Release"
       defines { "NDEBUG" }
       optimize "On"